const express = require('express')
const taskController = require('../controllers/taskController')

const router = express.Router()

router.get('/:id', (req, res) => {
    taskController.getTask(req.params.id).then(result => res.send(result))
})

router.put('/:id/:status', (req, res) => {
    taskController.updateStatus(req.params.id, req.params.status)
        .then(result => res.send(result))
})

module.exports = router