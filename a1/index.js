const express = require('express')
const mongoose = require('mongoose')
const taskRoutes = require('./routes/taskRoutes')

// server setup
const app = express()
const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// database connection
// connecting to mongodb atlas
mongoose.set('strictQuery', true);

mongoose.connect('mongodb+srv://admin:admin123@clusterbatch248.9omufck.mongodb.net/s36?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// connection listener
let db = mongoose.connection
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("We're connected to MongoDB Atlas!"))

app.use('/tasks', taskRoutes)

app.listen(port, ()=>console.log(`Server running at port ${port}`))