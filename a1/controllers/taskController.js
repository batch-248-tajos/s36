const Task = require('../models/Task')

module.exports.getTask = (id) => {
    return Task.findById(id).then((foundTask, err) => {
        if (err) {
            return console.error(err)
        }

        return foundTask
    })
}

module.exports.updateStatus = (id, newStatus) => {
    return Task.findById(id).then((task, err) => {
        if (err) {
            return console.log(err)
        }

        task.status = newStatus

        return task.save().then((savedTask, err) => {
            if (err) {
                return console.log(err)
            }

            return savedTask
        })
    })
}