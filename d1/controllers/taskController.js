const Task = require('../models/Task')

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
}

module.exports.createTask = (requestBody) => {
	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client/Postman
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if (error) {
			console.log(error);
			// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// The else statement will no longer be evaluated
			return false;
		// Save successful, returns the new task object back to the client/Postman
		} else {
			return task;
		}
	})

}

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
        if (err) {
            console.log(err)
            return false
        } else {
            return removedTask
        }

    })

    Task.find
}

module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error)
            return false
        }

        result.name = newContent

        return result.save().then((updatedTask, err) => {
            if (err) {
                console.log(err)
                return false
            } else {
                return updatedTask
            }
        })
    })
}