// contains all the endpoints for our application
// we separate the routes such as that index.js 
// only contains info on the server
const express = require('express')
const taskController = require('../controllers/taskController')
// makes it easier to create routes for our application
const router = express.Router()

router.get("/",(req,res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


router.post("/", (req, res) => {

	// The "createTask" function needs the data from the request body, so we need to supply it to the function
	// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	//taskController.createTask(req.body);
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	
})

router.delete("/:id", (req, res) => {
    taskController.deleteTask(req.params.id).then(result => res.send(result))
})

router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body.name).then(resultFromController => res.send(resultFromController));
})

module.exports = router